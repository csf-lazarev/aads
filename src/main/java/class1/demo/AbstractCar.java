package class1.demo;

/**
 * Created by lazarev_ya_v on 20.02.2023.
 */
public abstract class AbstractCar {

    protected String brand;
    protected String model;
    protected int weight;
    protected int horsePower;

    public AbstractCar(String brand, String model, int weight, int horsePower) {
        this.brand = brand;
        this.model = model;
        this.weight = weight;
        this.horsePower = horsePower;
    }

    public AbstractCar(String brand, String model) {
        this.brand = brand;
        this.model = model;
        this.weight = 1000;
        this.horsePower = 240;
    }

    public AbstractCar() {
        this("No brand", "No model");
    }

    public abstract void startEngine();

    public abstract void beep();

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return "AbstractCar{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", weight=" + weight +
                ", horsePower=" + horsePower +
                '}';    }
}
