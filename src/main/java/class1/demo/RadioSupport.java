package class1.demo;

/**
 * Created by lazarev_ya_v on 20.02.2023.
 */
public interface RadioSupport {
    default void turnRadioOn() {
        System.out.println("PSHHHHH");
    }
    void turnRadioOff();
}
