package class1.demo;

public class Main {
    public static void main(String[] args) {
        AbstractCar ladaSedan = new PassengerCar("Lada", "Priora", 1000, 106, "Sedan");
        ladaSedan.setHorsePower(200);
        System.out.println(ladaSedan);
        ladaSedan.startEngine();
        ladaSedan.beep();
        RadioSupport ladaWithRadio = (RadioSupport) ladaSedan;
        ladaWithRadio.turnRadioOn();
        ladaWithRadio.turnRadioOff();
        System.out.println("===================================");
        AbstractCar kamaz = new CargoCar("Kamaz", "4x4", 7000, 210, 5000);
        kamaz.startEngine();
        kamaz.beep();
        System.out.println(kamaz.toString());
    }
}
