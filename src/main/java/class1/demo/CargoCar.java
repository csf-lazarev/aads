package class1.demo;

/**
 * Created by lazarev_ya_v on 20.02.2023.
 */
public class CargoCar extends AbstractCar {

    private int maxCargoWeight;

    public CargoCar(String brand, String model, int weight, int horsePower) {
        super(brand, model, weight, horsePower);
    }

    public CargoCar(String brand, String model, int weight, int horsePower, int maxCargoWeight) {
        super(brand, model, weight, horsePower);
        this.maxCargoWeight = maxCargoWeight;
    }

    public int getMaxCargoWeight() {
        return maxCargoWeight;
    }

    public void setMaxCargoWeight(int maxCargoWeight) {
        this.maxCargoWeight = maxCargoWeight;
    }

    @Override
    public void startEngine() {
        System.out.println("GRGJAJAYGSUHGYFGTYGUIDUI");
    }

    @Override
    public void beep() {
        System.out.println("BBBBBBBBBEEEEEEEEEEEEEP BEEEEEP");
    }

    @Override
    public String toString() {
        return "CargoCar{" +
                "maxCargoWeight=" + maxCargoWeight +
                "} " + super.toString();
    }
}
