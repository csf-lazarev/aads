package class1.demo;

/**
 * Created by lazarev_ya_v on 20.02.2023.
 */
public class PassengerCar extends AbstractCar implements RadioSupport, LockDoorSupport {

    private String bodyType;

    public PassengerCar(String brand, String model, int weight, int horsePower) {
        super(brand, model, weight, horsePower);
    }

    public PassengerCar(String brand, String model, int weight, int horsePower, String bodyType) {
        super(brand, model, weight, horsePower);
        this.bodyType = bodyType;
    }

    private void startEngineBlackMagic() {
        System.out.println("Do black magic");
    }

    @Override
    public void startEngine() {
        startEngineBlackMagic();
        System.out.println("DRRRRR");
    }

    @Override
    public void beep() {
        System.out.println("BEEEP");
    }

    @Override
    public String toString() {
        return "PassengerCar{" +
                "bodyType='" + bodyType + '\'' +
                "} " + super.toString();
    }

    @Override
    public void turnRadioOn() {
        System.out.println("RADIO MIR");
    }

    @Override
    public void turnRadioOff() {
        System.out.println("Goodbye");
    }

    @Override
    public void lockDoors() {
        System.out.println("Doors locked. It's a TRAP!");
    }
}
