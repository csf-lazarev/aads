package class1.task9;

public class Demo {
    public static void main(String[] args) {
        Student student = new Student();
        student.getSemester(1).addSubjectScore("IiP", 4);
        student.getSemester(1).addSubjectScore("MA", 5);
        student.getSemester(1).addSubjectScore("History", 3);
        student.getSemester(2).addSubjectScore("MLiTA", 4);
        student.getSemester(2).addSubjectScore("Web", 2);
        System.out.println(student.getSemesterAverage(1));
        System.out.println(student.getSemesterAverage(2));
        System.out.println(student.getFullAverage());
    }
}
