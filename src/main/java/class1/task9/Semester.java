package class1.task9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Semester {

    private Map<String, Integer> subjectScore = new HashMap<>();

    public void addSubjectScore(String subject, int score) {
        subjectScore.put(subject, score);
    }

    public int getSubjectScore(String subject) {
        return subjectScore.getOrDefault(subject, 0);
    }

    public double averageScore() {
        Collection<Integer> values = subjectScore.values();
        int size = values.size();
        int sum = 0;
        for (Integer value : values) {
            sum += value;
        }
        if (size == 0) {
            return 0;
        }
        return ((double) sum) / size;
    }

    public Collection<Integer> getScoresList() {
        return subjectScore.values();
    }
}
