package class1.task9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Student {

    private Map<Integer, Semester> semesterMap = new HashMap<>();

    public Semester getSemester(int semester) {
        doSemesterValidate(semester);
//        return semesterMap.computeIfAbsent(semester, integer -> new Semester());
        if (!semesterMap.containsKey(semester)) {
            semesterMap.put(semester, new Semester());
        }
        return semesterMap.get(semester);
    }

    public double getSemesterAverage(int semester) {
       doSemesterValidate(semester);
       return getSemester(semester).averageScore();
    }

    public double getFullAverage(){
        int sum = 0;
        int count = 0;
        for (Integer semester : semesterMap.keySet()) {
            for (Integer score : semesterMap.get(semester).getScoresList()) {
                sum += score;
                count++;
            }
        }
        if (count == 0) {
            return 0;
        }
        return ((double) sum) / count;
//        return semesterMap.entrySet().stream()
//            .flatMap(e -> e.getValue().getScoresList().stream())
//            .mapToInt(value -> value)
//            .summaryStatistics()
//            .getAverage();
    }

    private void doSemesterValidate(int semester) {
        if (semester < 1 || semester > 8) {
            throw new IllegalArgumentException("Bad semester number");
        }
    }
}
