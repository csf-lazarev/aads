package class1.task26;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack<T> {

    private static final int INITIAL_CAPACITY = 10;
    private T[] storage;

    private int size = 0;

    public ArrayStack(){
        storage = (T[]) new Object[INITIAL_CAPACITY];
    }

    public ArrayStack(int initialCapacity) {
        storage = (T[]) new Object[initialCapacity];
    }

    public boolean push(T value){
        ensureCapacity(size + 1);
        storage[size] = value;
        size++;
        return true;
    }

    public T pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack is empty");
        }
        T value = storage[size - 1];
        size--;
        return value;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }
    private void ensureCapacity(int cap) {
        if (storage.length < cap) {
            storage = Arrays.copyOf(storage, storage.length * 2);
            System.out.println("Stack capacity has been increased up to " + storage.length);
        }
    }

}
