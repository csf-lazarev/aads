package class1.task26;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Demo {
    public static void main(String[] args) {
        ArrayStack<Integer> stack = new ArrayStack<>();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        try {
            System.out.println(stack.pop());
        } catch (NoSuchElementException e) {
            System.out.println(e.getMessage());
        }

        ArrayStack<Integer> stack1 = new ArrayStack<>(5);
        IntStream.range(0, 27).forEach(stack1::push);
        System.out.println("=======================");

        while (!stack1.isEmpty()) {
            System.out.print(stack1.pop() + " ");
        }
    }
}
