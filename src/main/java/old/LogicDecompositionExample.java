package old;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LogicDecompositionExample {

    private static final double EPS = 0.001;

    public static void main(String[] args) {
        int x1 = scanInt("Input x1");
        int y1 = scanInt("Input y1");
        int r1 = scanInt("Input r1", 1, Integer.MAX_VALUE);
        int x2 = scanInt("Input x2");
        int y2 = scanInt("Input y2");
        int r2 = scanInt("Input r2", 1, Integer.MAX_VALUE);
        System.out.println(getCirclesPlaceInfo(x1, y1, r1, x2, y2, r2));
    }

    public static int scanInt(String before, int min, int max) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.printf("%s: ", before);
            try {
                int value = scanner.nextInt();
                if (value < min || value > max) {
                    System.out.printf("Entered value out of bounds [%d, %d]\n", min, max);
                    continue;
                }
                return value;
            } catch (InputMismatchException e) {
                System.out.println("Wrong number format. Try again.");
                scanner.nextLine();
            }
        }
    }

    public static int scanInt(String before) {
        return scanInt(before, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static boolean isDoubleEquals(double a, double b) {
        return Math.abs(a - b) < EPS;
    }

    public static String getCirclesPlaceInfo(int x1, int y1, int r1, int x2, int y2, int r2) {
        double d = distanceBetweenPoints(x1, y1, x2, y2);
        double placementFeature = placementFeature(d, r1, r2);
        double intersectionFeatureOne = intersectionFeatureOne(d, r1, r2);
        double intersectionFeatureTwo = intersectionFeatureTwo(d, r1, r2);

        if (intersectionFeatureOne > 0 || isDoubleEquals(intersectionFeatureOne, 0)) {
            if (isDoubleEquals(placementFeature, 0)) { // intersect inside
                return "circles are touching inside";
            } else if (placementFeature > 0) {
                return "circles haven't intersection, circle inside other one";
            } else {
                return "circles have intersection inside";
            }
        } else {
            if (isDoubleEquals(intersectionFeatureTwo, 0)) {
                return "circles have touching outside";
            } else if (intersectionFeatureTwo > 0) {
                return  "circles have intersection outside";
            } else {
                return "circles haven't intersection, no any circle inside other one";
            }
        }
    }

    public static double placementFeature(double d, int r1, int r2) {
        if (r2 > r1) {
            int t = r2;
            r2 = r1;
            r1 = t;
        }
        return r1 - d - r2;
    }

    public static double intersectionFeatureOne(double d, int r1, int r2) {
        if (r2 > r1) {
            r1 = r2;
        }
        return r1 - d;
    }

    public static double intersectionFeatureTwo(double d, int r1, int r2) {
        return r1 + r2 - d;
    }

    public static double distanceBetweenPoints(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }
}
