package class2;

import class2.secret.SimpleDoubleLinkedList;
import class2.secret.SimpleLinkedList;

import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<String> strings = new SimpleDoubleLinkedList<>();
        strings.add("A");
        strings.add("B");
        strings.add(1, "C");

        logList("Actual list", strings);
        log("Last element of list", strings.get(2));

        strings.set(2, "Y");

        logList("Last should be 'Y'", strings);
        log("Contains 'A'", strings.contains("A"));
        log("Contains 'X'", strings.contains("X"));

        strings.add("A");

        logList("Actual list, last new A", strings);
        log("First index of 'A'",strings.indexOf("A"));
        log("Last index of 'A'",strings.lastIndexOf("A"));

        strings.remove("Y");

        logList("Now 'Y' should be removed", strings);

        strings.remove("A");

        logList("First 'A' should be removed", strings);

        String removed = strings.remove(1);

        logList("Last element should be removed", strings);
        log("Removed element", removed);

        try {
            strings.remove(1);
        } catch (IndexOutOfBoundsException e) {
            log("Out of bound", 1);
        }

        strings.remove(0);

        logList("Should be empty now", strings);

        "12345678".chars().mapToObj(c -> ((char) c)).map(String::valueOf).forEach(strings::add);

        logList("Should be list from 1 to 8", strings);

        strings.remove(0);

        logList("Should start from 2", strings);

        strings.remove("7");

        logList("Should not contains '7'", strings);

        strings.set(5, "100");

        logList("8 became 100", strings);

    }

    private static void logList(String message, List list) {
        System.out.printf("%s: %s\n\n", message, String.join(", ", list));
    }

    private static void log(String message, Object o) {
        System.out.printf("%s: %s\n\n", message, o);
    }
}
