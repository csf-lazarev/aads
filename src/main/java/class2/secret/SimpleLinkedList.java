package class2.secret;

import java.util.*;

public class SimpleLinkedList<T> implements List<T> {

    protected class Entry {
        public T value;
        public Entry next;

        public Entry() {
            value = null;
            next = null;
        }

        public Entry(T value, Entry next) {
            this.value = value;
            this.next = next;
        }
    }

    protected Entry tail = null;
    protected Entry head = null;

    private int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Entry curr = head;
        while (curr != null) {
            if (curr.value.equals(o)) {
                return true;
            }
            curr = curr.next;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Entry current = head;
            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (current == null) {
                    throw new NoSuchElementException();
                }
                T value = current.value;
                current = current.next;
                return value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean add(T t) {
        addToTail(t);
        return true;
    }

    private void resize(int newSize) {
        throw new UnsupportedOperationException("This operation does not supported for LinkedList");
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) {
            return false;
        }
        Entry prev = null;
        Entry curr = head;
        while (curr != null && !curr.value.equals(o)) {
            prev = curr;
            curr = curr.next;
        }
        if (curr == null) {
            return false;
        }
        if (prev == null) { // it''s head
            head = curr.next;
        } else {
            prev.next = curr.next;
            if (curr == tail) {
                tail = prev;
            }
        }
        size--;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::addToTail);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        Entry iter = head;
        while (index > 0) {
            iter = iter.next;
            index--;
        }
        return iter.value;
    }

    @Override
    public T set(int index, T element) {
        validateIndex(index);
        Entry iter = head;
        while (index > 0) {
            iter = iter.next;
            index--;
        }
        T oldValue = iter.value;
        iter.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, T element) {
        addTo(element, index);
    }

    @Override
    public T remove(int index) {
        validateIndex(index);
        if (index == 0) {
            T value = head.value;
            head = head.next;
            size--;
            return value;
        } else {
            Entry prev = head;
            Entry curr = head.next;
            while (index > 1) {
                prev = curr;
                curr = curr.next;
                index--;
            }
            T value = curr.value;
            prev.next = curr.next;

            // If we delete the tail element, we should update pointer to tail
            if (tail == curr) {
                tail = prev;
            }

            size--;
            return value;
        }
    }

    @Override
    public int indexOf(Object o) {
        Entry iter = head;
        int index = 0;
        while (iter != null && !iter.value.equals(o)) {
            iter = iter.next;
            index++;
        }
        if (iter == null) {
            return -1;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        Entry iter = head;
        int index = 0;
        int ret = -1;
        while (iter != null) {
            if (iter.value.equals(o)) {
                ret = index;
            }
            iter = iter.next;
            index++;
        }
        return ret;
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(String.format("Index %d out of bounds [%d, %d]", index, 0, Math.min(0, size - 1)));
        }
    }

    private Entry addToTail(T value) {
        Entry entry = new Entry(value, null);
        if (head == null) {
            head = entry;
            tail = entry;
        } else {
            tail.next = entry;
        }
        tail = entry;
        size++;
        return entry;
    }

    private Entry addToHead(T value) {
        Entry entry = new Entry(value, null);
        if (head == null) {
            head = entry;
            tail = entry;
        } else {
            entry.next = head;
            head = entry;
        }
        size++;
        return entry;
    }

    private Entry addTo(T value, int index) {
        validateIndex(index);
        if (index == size) {
            return addToTail(value);
        }
        if (index == 0) {
            return addToHead(value);
        }
        Entry iter = head;
        while (index > 1) {
            iter = iter.next;
            index--;
        }
        Entry newEntry = new Entry(value, iter.next);
        iter.next = newEntry;
        size++;
        return newEntry;
    }
}
