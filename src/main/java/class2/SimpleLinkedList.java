package class2;

import java.util.*;

public class SimpleLinkedList<T> implements List<T> {

    private class Entry {
        T value;
        Entry next;

        public Entry() {
            value = null;
            next = null;
        }

        public Entry(T value, Entry next) {
            this.value = value;
            this.next = next;
        }
    }

    private Entry tail = null;
    private Entry head = null;

    private int size = 0;

    /**
     * Just return size variable
     * @return
     */
    @Override
    public int size() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through elements and compare with .equals(o) method each
     * If we get matching e.g. equals return true, return true
     * If we pass all list and didn't find any matching, return false
     *
     * @param o element whose presence in this list is to be tested
     * @return
     */
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * This nethod allows use our list in the code as iterable structure
     * After we implement this we can use for(E elem: list) loop, cat use list.stream and etc.
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Entry current = head;
            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (current == null) {
                    throw new NoSuchElementException();
                }
                T value = current.value;
                current = current.next;
                return value;
            }
        };
    }

    /**
     * May leave unimplemented
     * @return
     */
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * May leave unimplemented
     * @param a the array into which the elements of this list are to
     *          be stored, if it is big enough; otherwise, a new array of the
     *          same runtime type is allocated for this purpose.
     * @return
     * @param <T1>
     */
    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * We should add new element to tail of our list
     * Here 'tail' pointer is useful
     * @param t element whose presence in this collection is to be ensured
     * @return
     */
    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Leave unimplemented
     * @param newSize
     */
    private void resize(int newSize) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * We should iterate through our list and find first matching with 'o'
     * If we found it, we have 3 items: current with matching value, previous - item that before current
     * and next - item next current
     * We just update links: previous.next = current.next
     * Don't forget decrement size of the list
     * Also pay attention for cases when we delete head or tail - we should update these pointers
     *
     * @param o element to be removed from this list, if present
     * @return
     */
    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * may leave unimplemented except cases when you need this method in your task
     *
     * @param c collection to be checked for containment in this list
     * @return
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Just iterate through collection and add all elements to tail
     *
     * @param c collection containing elements to be added to this collection
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * May leave unimplemented
     *
     * @param index index at which to insert the first element from the
     *              specified collection
     * @param c collection containing elements to be added to this list
     * @return
     */
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * May leabe unimplemenetd
     * @param c collection containing elements to be removed from this list
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * May leave unimplemented
     * @param c collection containing elements to be retained in this list
     * @return
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Should remove all elements from list
     * Tip: you don't need to remove each element, update head and tail pointers is enough
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not implemented yet");
    }


    /**
     * Iterate through list and find item with given index
     * You should start from head and then use 'next link' until index is reached
     * Remember about Index Out Of Bound problem: add validation of index
     *
     * @param index index of the element to return
     * @return
     */
    @Override
    public T get(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through list and find item with given index, then update it value
     * You should start from head and then use 'next link' until index is reached
     * Remember about Index Out Of Bound problem: add validation of index
     *
     * @param index index of the element to replace
     * @param element element to be stored at the specified position
     * @return
     */
    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through list and find item with given index
     * You should start from head and then use 'next link' until index is reached
     * Think about what else we need to have to perform this operation(tip: not only item with index 'index')
     * Remember about Index Out Of Bound problem: add validation of index
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     */
    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through list and find item with given index
     * You should start from head and then use 'next link' until index is reached
     * Think about what else we need to have to perform this operation(tip: not only item with index 'index')
     * Then update links between necessary items
     * Remember about Index Out Of Bound problem: add validation of index
     *
     * @param index the index of the element to be removed
     * @return
     */
    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through list and compare item value with 'o' by equals method
     * If we found matching, just return index of this item. You should create counter for this and increment it during iteration
     * If we didn't find matching, return -1
     *
     * @param o element to search for
     * @return
     */
    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Iterate through list and compare item value with 'o' by equals method
     * If we found matching, remember index of this item. You should create counter for this and increment it during iteration
     * You need variable to keep last index of 'o'
     * If we didn't find matching, return -1
     *
     * @param o element to search for
     * @return
     */
    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Leave unimplemented
     * @return
     */
    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Leave unimplemented
     * @param index index of the first element to be returned from the
     *        list iterator (by a call to {@link ListIterator#next next})
     * @return
     */
    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Leave unimplemented if you don't need this method
     *
     * If you implement this method, you should create NEW list with new ENTRIES, do not assign already created items
     * Iterate through list, find item with index fromIndex, then add each value until toIndex is reached
     * Remember about Index Out Of Bound problem: add validation of indices
     *
     * @param fromIndex low endpoint (inclusive) of the subList
     * @param toIndex high endpoint (exclusive) of the subList
     * @return
     */
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
