package class2.task4;

import class2.secret.SimpleLinkedList;

public class MaxCounterList<T extends Comparable<T>> extends SimpleLinkedList<T> {
    public int getMaximumCount() {
        if (isEmpty()) {
            return 0;
        }
        if (size() == 1) {
            return 1;
        }
        int count = 1;
        T value = head.value;
        Entry curr = head.next;
        while (curr != null) {
            int compare = value.compareTo(curr.value);
            if (compare == 0) {
                count++;
            } else if (compare < 0) {
                count = 1;
                value = curr.value;
            }
            curr = curr.next;
        }
        return count;
    }
}
