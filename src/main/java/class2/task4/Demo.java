package class2.task4;

public class Demo {
    public static void main(String[] args) {
        MaxCounterList<Integer> integers = new MaxCounterList<>();
        integers.add(1);
        integers.add(2);
        integers.add(1);
        integers.add(2);
        integers.add(5);
        integers.add(3);
        integers.add(5);
        integers.add(5);
        integers.add(1);
        integers.add(5);
        System.out.println(integers.getMaximumCount());
    }
}
