package class4;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) {
        List<Integer> list = new Random(Instant.now().toEpochMilli())
                .ints(12, 10, 40)
                .boxed()
                .collect(Collectors.toList());

        QuickSort<Integer> sorter = QuickSort.with(Comparator.<Integer>naturalOrder());
        sorter.sort(list);
        System.out.println(list);
    }
}
