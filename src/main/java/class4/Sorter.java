package class4;

import java.util.List;

public interface Sorter<T>{
    void sort(List<T> list);
}
