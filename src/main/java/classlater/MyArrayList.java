package classlater;

import java.util.*;

public class MyArrayList<T> implements List<T> {

    private static int INITIAL_CAPACITY = 4;

    private int size;
    private T[] storage;

    public MyArrayList() {
        storage = (T[]) new Object[INITIAL_CAPACITY];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean add(T t) {
        if (size == storage.length) {
            resize(size * 2);
        }
        storage[size] = t;
        size++;
        return true;
    }

    private void resize(int newSize) {
        if (size > newSize) {
            newSize = size * 2;
        }
        storage = Arrays.copyOf(storage, newSize);
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public T get(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException();
        }
        return storage[index];
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
